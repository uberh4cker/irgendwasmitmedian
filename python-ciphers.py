#!/usr/bin/env python

import sys, socket, ssl

target = sys.argv[1]
port = 443


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
ssl_sock = ssl.wrap_socket(s,cert_reqs=ssl.CERT_REQUIRED,ca_certs='/etc/ssl/certs/ca-certificates.crt')
ssl_sock.connect((target, port))
print repr(ssl_sock.getpeername())
print ssl_sock.cipher()
