#!/usr/bin/env python

import socket

TCP_IP = '127.0.0.1'
TCP_PORT = 5006
BUFFER_SIZE = 1024

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((TCP_IP, TCP_PORT))
s.listen(1)

while True:
    conn, _ = s.accept()
    conn.settimeout(5)
    try:
        data = conn.recv(BUFFER_SIZE)
    except socket.timeout:
        pass

    conn.send(b'polo' if b'marko' in data.lower() or b'marco' in data.lower() else b'pong')
    conn.close()
